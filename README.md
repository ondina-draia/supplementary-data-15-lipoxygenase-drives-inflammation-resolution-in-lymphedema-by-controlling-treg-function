# (Supplementary data) 15-Lipoxygenase drives inflammation resolution in lymphedema by controlling Treg function

All the Supplementary data is available here for the publication 

15-Lipoxygenase drives inflammation resolution in lymphedema by controlling Treg function: 

Zamora A.1, Nougué M.1, Verdu L.1, Balzan E.1, Draia-Nicolau O.1, Benuzzi E.1, Pujol F.1, Baillif V. 2, Lacazette E.1, Morfoisse F.1, Galitzky J.1, Bouloumié A.1, Dubourdeau M.2, Chaput B.3, Fazilleau N. 4, Malloizel-Delauney J.5, Bura-Rivière A.5, Prats AC.1, Garmy-Susini B1*.

1 I2MC, Université de Toulouse, Inserm UMR 1297, UT3, Toulouse, France.
2 Ambiotis SAS, Toulouse, France.
3 Service de Chirurgie Plastique et des Brûlés, Centre Hospitalier Universitaire de Toulouse, Toulouse, France.
4 Infinity, Toulouse Institute for Infectious and Inflammatory Diseases, Inserm UMR1291, CNRS UMR5051, University of Toulouse, 31024 Toulouse, France.
5 Service de Médecine Vasculaire, Centre Hospitalier Universitaire de Toulouse, Toulouse, France.

*Corresponding author. 
Contact information : Barbara Garmy-Susini, Inserm UMR 1297, Institut des Maladies Métaboliques et Cardiovasculaires, 1, Avenue Jean Poulhes, BP 84225, 31432 Toulouse cedex 4, France (e-mail : barbara.garmy-susini@inserm.fr)


